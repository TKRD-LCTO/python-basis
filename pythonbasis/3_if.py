# coding: utf-8

a = 0
b = 3

if a == 0:
    print("a is zero")
elif a == 1:
    print("a is 1")
else:
    print("a is False")

if a == 0 and b == 3:
    print("a is zero and b is 3")
elif not a == 0 or not b == 3:
    print("statement matched")