# coding: utf-8

print("Hello,Python")
# コメントは先頭に半角シャープマーク


if __name__ == "__main__":
    # 括弧{}の代わりにインデントで階層を調整
    print("Hello,World")

    # Python 2.xの場合の書き方
    # print "Hello,World"
