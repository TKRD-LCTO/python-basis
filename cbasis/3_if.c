/*
 * 3_if.c
 *
 *  Created on: 2018/09/10
 *      Author: yutakarada
 */


#include<stdio.h>

int main(void){
	int a = 0;
	int b = 3;

	if(a==0){
		printf("a is zero¥n");
	}
	else if(a==1){
		printf("a is 1¥n");
	}
	else{
		printf("a is other¥n");
	}

	//switch文
	switch(a){
	case 0:
		printf("a is zero.¥n");
		break;
	case 1:
		printf("a is 1.¥n");
		break;
	default:
		printf("a is other.¥n");
		break;
	}

	//and or
	if(a==0 && b==3){
		printf("a is zero and b is 3¥n");
	}
	else if(a!=0 || b!=3){
		printf("statement matched¥n");
	}

}
